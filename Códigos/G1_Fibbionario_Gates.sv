`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//Team: 3
// Create Date: 05.04.2024 15:38:32
// Experiencie: Guia 2
// Module Name: Fibbionario
// Description: Se encarga de dar un 1 en la salida F si se cumple la condicion de que no hayan 1's consecutivos en A,B,C,D
// En caso contrario, la salida F sera 0
// Ejemplo: A=1, B=0, C=1, D=0 -> F=1
//          A=1, B=1, C=0, D=0 -> F=0

// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Fibbionario(
    input logic A,
    input logic B,
    input logic C,
    input logic D,
    output logic F
    );
    
    assign F = (~A&~C)|(~B&~C)|(~B&~D);

endmodule













