`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiencie: Guia 3
// Team: 3
// Create Date: 21.04.2024 21:27:52 
// Module Name: Divisor_frecuenciaN
// Description: Divisor de frecuencia: Se encarga de dividir la frecuencia de entrada en una frecuencia de salida deseada
//              Solo puede dividir la frecuencia de entrada en una cantidad entera, multiplo de 2, de la frecuencia de salida
//              Ej: Si la frecuencia de entrada es de 100Mh, puede dar una frecuencia de salida de 50Mhz, 25Mhz, 12Mhz( deberia ser 12.5 pero no soporta numeros decimales), etc
//              Parametros: Frecuencia de entrada y frecuencia de salida
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Divisor_frecuencia #(parameter CLK_IN_FREQ_MHZ, parameter CLK_OUT_FREQ_MHZ)
  (
    input logic clk_in,
    input logic reset,
    output logic clk_out
  );

  // Calculamos el n�mero de ciclos de reloj necesarios para la frecuencia de salida deseada
  // F�rmula: CLK_OUT_CYCLES = CLK_IN_FREQ_MHZ / 2* CLK_OUT_FREQ_MHZ
  parameter COUNTER_MAX = (CLK_IN_FREQ_MHZ) / (2*CLK_OUT_FREQ_MHZ);
  
  // Ancho de la cuenta basado en el n�mero de ciclos de reloj
    localparam DELAY_WIDTH = $clog2(COUNTER_MAX);

  
  // Contador de DELAY_WIDTH bits
  logic [DELAY_WIDTH-1:0] counter = 0;

  // Resetea el contador e invierte el valor del reloj de salida
  always_ff @(posedge clk_in) begin
    if (reset == 1'b1) begin
      // Reset s�ncrono, se setea el contador y la salida a un valor conocido
      counter <= 0;
      clk_out <= 0;
    end
    else if (counter == COUNTER_MAX-1) begin
      // Se resetea el contador y se invierte la salida
      counter <= 0;
      clk_out <= ~clk_out;
    end
    else begin
      // Se incrementa el contador y se mantiene la salida con su valor anterior
      counter <= counter + 1'b1;
      clk_out <= clk_out;
    end
  end

endmodule