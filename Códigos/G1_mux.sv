`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience: Guia 1
// Create Date: 05.04.2024 16:40:28
// Module Name: mux
// Description: Este modulo se encarga de seleccionar una de las 4 entradas y enviarla a la salida 

// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux(
    input logic [3:0] A, B, C, D,
    input logic [1:0] sel,
    output logic [3:0] out
    );
    
    always_comb 
    begin
        case(sel)
            2'b00 : out = A;
            2'b01 : out = B;
            2'b10 : out = C;
            default : out = D;
        endcase
   end 
endmodule 