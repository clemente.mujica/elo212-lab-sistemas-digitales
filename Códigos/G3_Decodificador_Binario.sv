`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiense: Guia 3
// Team: 3
// Create Date: 19.04.2024 08:09:57
 
// Module Name: Decodificador_Binario

// Description: Funciona como un ONE HOT, recibe un numero binario de 2 bits y calcula un 2^N bits de salida, donde N es el numero decimal en los bits de entrada 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// N = 2
module Decodificador_Binario(
    input logic [1:0] a,
    output logic [3:0] d
    );
    
    always_comb
    begin
        case(a)
            2'b00 : d = 4'b0001;
            2'b01 : d = 4'b0010;
            2'b10 : d = 4'b0100;
            2'b11 : d = 4'b1000;
            default : d = 4'b0000; // No es necesario pero por siacaso
        endcase
    end                       
endmodule
