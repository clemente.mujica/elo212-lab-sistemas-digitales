`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2024 18:45:27
// Design Name: 
// Module Name: sumador_2bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sumador_2bit(
    input logic [1:0] a, b, 
    input logic cin,
    output logic suma, acarreo
);
    
assign {acarreo, suma} = a + b + cin;
endmodule
