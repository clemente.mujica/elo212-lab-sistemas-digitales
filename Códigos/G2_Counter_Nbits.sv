`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience : Guia 2
// Team : 3
// Create Date: 14.04.2024 21:14:31
// Module Name: counter_Nbits
// Description: Este modulo se encarga de contar de 0 a N-1, donde N es un parametro de entrada.

// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_Nbits #(parameter N = 4)(
    input logic clk, reset,
    output logic [N-1:0] count
);

    always_ff @(posedge clk) begin
        if(reset)
            count <= {N{1'b0}}; // Reinicia el contador con N bits en 0
        else
            count <= count + 1;
    end

endmodule
