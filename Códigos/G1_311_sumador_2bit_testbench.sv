`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2024 19:02:54
// Design Name: 
// Module Name: sumador_2bit_testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sumador_2bit_testbench();
    logic [1:0] a, b, cin;
    logic suma, acarreo;
    
    sumador_2bit w(
    .a(a),
    .b(b),
    .cin(cin),
    .suma(suma),
    .acarreo(acarreo)
    );
    
    initial
        begin
        a = 2'b0;
        b = 2'b0;
        cin = 2'b0;
        #3
        a = 2'b0;
        b = 2'b0;
        cin = 2'b1;
        #3
        a = 2'b0;
        b = 2'b1;
        cin = 2'b0;
        #3
        a = 2'b0;
        b = 2'b1;
        cin = 2'b1;
        #3
        a = 2'b1;
        b = 2'b0;
        cin = 2'b0;
        #3
        a = 2'b1;
        b = 2'b0;
        cin = 2'b1;
        #3
        a = 2'b1;
        b = 2'b1;
        cin = 2'b0;
        #3
        a = 2'b1;
        b = 2'b1;
        cin = 2'b1;
        #3
        a = 2'b1;
        b = 2'b1;
        cin = 1'b1;
        #3
        a = 2'b1;
        b = 2'b2;
        cin = 2'b1;
        #3
        a = 2'b1;
        b = 2'b2;
        cin = 2'b2;
        #3
        a = 2'b2;
        b = 2'b1;
        cin = 2'b1;
        #3
        a = 2'b2;
        b = 2'b1;
        cin = 2'b2;
        #3
        a = 2'b2;
        b = 2'b2;
        cin = 2'b1;
        #3
        a = 2'b2;
        b = 2'b2;
        cin = 2'b2;
        #3
        a = 2'b2;
        b = 2'b2;
        cin = 2'b3;
        #3
        a = 2'b2;
        b = 2'b3;
        cin = 2'b2;
        #3
        a = 2'b2;
        b = 2'b3;
        cin = 2'b3;
        #3
        a = 2'b3;
        b = 2'b2;
        cin = 2'b2;
        #3
        a = 2'b3;
        b = 2'b2;
        cin = 2'b3;
        #3
        a = 2'b3;
        b = 2'b3;
        cin = 2'b2;
        #3
        a = 2'b3;
        b = 2'b3;
        cin = 2'b3;
        end
endmodule
