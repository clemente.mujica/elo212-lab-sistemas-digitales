`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Team 3
// Experience: Guia 3

// Create Date: 12.04.2024 14:53:53
// Module Name: BCD_to_sevenSeg

// Description: Este modulo convierte un numero BCD de 4 bits Hexadecimal a una señal para un display de 7 segmentos
//              Catodo va a 0
//              Anodo va a las salidas logicas sevenSeg

// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module BCD_7seg16(
    input logic [3:0] BCD_in,
    output logic [6:0] sevenSeg
    );
    always_comb begin
        case(BCD_in)           //abcdefg
            15'd0 : sevenSeg = 7'b0000001; // output is abcdefg
            15'd1 : sevenSeg = 7'b1001111;
            15'd2 : sevenSeg = 7'b0010010;
            15'd3 : sevenSeg = 7'b0000110;
            15'd4 : sevenSeg = 7'b1001100;
            15'd5 : sevenSeg = 7'b0100100;
            15'd6 : sevenSeg = 7'b0100000;
            15'd7 : sevenSeg = 7'b0001111;
            15'd8 : sevenSeg = 7'b0000000;
            15'd9 : sevenSeg = 7'b0001100;
            15'd10 : sevenSeg = 7'b0001000; //A
            15'd11 : sevenSeg = 7'b1100000; //b
            15'd12 : sevenSeg = 7'b0110001; //C
            15'd13 : sevenSeg = 7'b1000010; //d
            15'd14 : sevenSeg = 7'b0110000; //E
            15'd15 : sevenSeg = 7'b0111000; //F
            default : sevenSeg = 7'b1001000; // |-|
       endcase
    end
endmodule
