`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Testbench for the numeros module
//////////////////////////////////////////////////////////////////////////////////

module tb_Fibonary_Decimal;

    // Inputs
    logic clock;
    logic reset;

    // Outputs
    logic fib;
    logic on_off;
    logic [6:0] bus;

    // Instantiate the Unit Under Test (UUT)
    Fibonary_Decimal lol (
        .clock(clock),
        .reset(reset),
        .fib(fib),
        .on_off(on_off),
        .bus(bus)
    );

    // Clock generation
    always #5 clock = ~clock; // Generate a clock with a period of 10ns

    // Initial block to simulate the test conditions
    initial begin
        // Initialize Inputs
        clock = 0;
        reset = 1;
        #15;              // Hold reset for a short time
        reset = 0;

        // Wait 100 ns for global reset to finish
        #100;
        
        // Add additional time if needed to observe behavior
        #100;
        
        // Finish the simulation
        $finish;
    end
    
endmodule