`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience : Guia 1
// Team: 3
// Create Date: 05.04.2024 17:40:02

// Module Name: mux310
// Description: Este modulo se encarga de seleccionar una de las 5 entradas y enviarla a la salida
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux310(
    input logic [3:0] A, B, C, D, E,
    input logic [2:0] sel,
    output logic [3:0] out
    );
    
    always_comb 
    begin
        case(sel)
            3'b000 : out = A;
            3'b001 : out = B;
            3'b010 : out = C;
            3'b011 : out = D;
            3'b100 : out = E;
            default : out = 4'b0000;
        endcase
   end 
endmodule 
