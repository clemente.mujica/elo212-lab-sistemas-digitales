`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// Team: 3
// Create Date: 05.04.2024 14:52:16
// Experience: Guia 1 
// Module Name: 3_3
 // Tool Versions: 1.0
// Description: Conexion basica de los cables

// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module cable(
    input logic A,
    output logic B
    );
    
    assign B = A;
    
endmodule
