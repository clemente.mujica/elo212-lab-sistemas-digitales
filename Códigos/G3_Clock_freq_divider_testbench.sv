`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience: Guia 3
// Team: 3
// Create Date: 22.04.2024 10:45:32
// Design Name: 
// Module Name: Clock_freq_divider_testbench

// Description: Testbench for the Clock_freq_divider module
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Clock_freq_divider_testbench();
    logic clock_in, reset, clock_out_50M, clock_out_30M, clock_out_10M, clock_out_1M;
    
    Clock_freq_divider Kiki(
        .clock_100M(clock_in),
        .reset(reset),
        .clock_out_50M(clock_out_50M), 
        .clock_out_30M(clock_out_30M), 
        .clock_out_10M(clock_out_10M), 
        .clock_out_1M(clock_out_1M)
    );
    
    always #5 clock_in = ~clock_in; // Generate a clock with a period of 10ns

    // Initial block to simulate the test conditions
    initial begin
        // Initialize Inputs
        clock_in = 0;
        reset = 1;
        #15;              // Hold reset for a short time
        reset = 0;

        // Wait 100 ns for global reset to finish
        #100;
        
        // Add additional time if needed to observe behavior
        #100;

    end
endmodule
