`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2024 18:22:57
// Design Name: 
// Module Name: sumador_1bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sumador_1bit(
    input logic a, b, cin,
    output logic suma, acarreo
);
    
assign {acarreo, suma} = a + b + cin;
endmodule
