`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiencie: Guia 2
// Team: 3
// Create Date: 12.04.2024 15:37:00 
// Module Name: test_counter

// Description:  Este modulo se encarga de probar el modulo counter de 4 bits

// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_counter();
    logic clk, reset;
    logic [3:0] count;
    counter_4bits DUT(
        .clk(clk),
        .reset(reset),
        .count(count)
        );
    always #5 clk = ~clk;
    
    initial begin
        clk = 0;
        reset = 1;
        #10 reset = 0;
    end
endmodule
