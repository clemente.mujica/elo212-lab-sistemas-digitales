`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiencie: Guia 2
// Team: 3
// Create Date: 12.04.2024 15:31:06 
// Module Name: counter_4bits
// Description: Este modulo se encarga de contar de 0 a 15 (19 numeros por ciclo, debido a que cuenta con 4 bits de salida)
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_4bits(
    input logic clk, reset,
    output logic [3:0] count
    );
    always_ff @(posedge clk) begin
        if(reset)
            count <= 4'b0;
        else
            count <= count+1;
    end
endmodule
