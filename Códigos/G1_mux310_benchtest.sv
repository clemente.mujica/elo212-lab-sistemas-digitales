`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Team: 3
// Experiencie: Guia 1
// Create Date: 05.04.2024 17:42:09
// Module Name: mux310_benchtest
// Description: Testbench para el modulo mux310 del archivo G1_mux310.sv
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux310_benchtest();
        logic [3:0] A,B,C,D,E,out;
        logic [2:0] sel;
        
        mux310 DUT(
        .A (A),
        .B (B),
        .C (C),
        .D (D),
        .E (E),
        .sel (sel),
        .out (out)
        );
        
        initial
        begin
        A = 4'b0101;
        B = 4'b0100;
        C = 4'b0011;
        D = 4'b0010;
        E = 4'b0001;
        sel = 3'b000;
        #3
        sel = 3'b001;
        #3
        sel = 3'b010;
        #3
        sel = 3'b011;
        #3
        sel = 3'b100;
        #3
        sel = 3'b101;
        #3
        sel = 3'b110;
        #3
        sel = 3'b111;
        end
endmodule

