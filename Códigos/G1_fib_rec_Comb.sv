`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience : Guia 1
// Create Date: 05.04.2024 16:07:44 
// Module Name: fib_rec 
// Description: Este modulo se encarga de verificar que no hayan 1's consecutivos en la entrada BCD_in
// Si no hay 1's consecutivos, la salida fib sera 1, en caso contrario, sera 0
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fib_rec(
    input logic [3:0] BCD_in,
    output logic      fib
    );
    
    always_comb 
    begin
        if(BCD_in == 4'd0 || BCD_in == 4'd1 || BCD_in == 4'd2 || BCD_in == 4'd4
        || BCD_in == 4'd5 || BCD_in == 4'd8 || BCD_in == 4'd9 || BCD_in == 4'd10)
            fib = 1;
        else
            fib = 0;
    end
endmodule
