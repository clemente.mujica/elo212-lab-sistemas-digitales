`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiens: Guia 2
// Team: 3
// Create Date: 14.04.2024 21:15:59
// Design Name: 
// Module Name: counterN_testbench
// Description: Testbench para el contador de N bits
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counterN_testbench;
    logic clk, reset;
    logic [4:0] count;
    counterN #(5) DUT(
        .clk(clk),
        .reset(reset),
        .count(count)
    );
    
    always #5 clk = ~clk;
    
    initial begin
        clk = 0;
        reset = 1;
        #10 reset = 0;
        
        // Simulaci�n por un tiempo
        #100;
        
        // Finalizar simulaci�n
    end
endmodule
