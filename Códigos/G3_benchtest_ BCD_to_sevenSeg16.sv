
`timescale 1ns / 1ps

// Experience: Guia 3
// Create Date: 19.04.2024 15:07:05
// Module Name: test_BCD_to_sevenSeg
// Description: Este modulo se encarga de probar el modulo BCD_to_sevenSeg16 


module test_BCD_to_sevenSeg;
    logic [3:0] BCD_in;
    logic [6:0] sevenSeg;

    BCD_7seg16 DUT (
        .BCD_in(BCD_in),
        .sevenSeg(sevenSeg)
    );

    initial begin
            BCD_in = 4'd0; #3;
            BCD_in = 4'd1; #3;
            BCD_in = 4'd2; #3;
            BCD_in = 4'd3; #3;
            BCD_in = 4'd4; #3;
            BCD_in = 4'd5; #3;
            BCD_in = 4'd6; #3;
            BCD_in = 4'd7; #3;
            BCD_in = 4'd8; #3;
            BCD_in = 4'd9; #3;
            BCD_in = 4'd10; #3;
            BCD_in = 4'd11; #3;
            BCD_in = 4'd12; #3;
            BCD_in = 4'd13; #3;
            BCD_in = 4'd14; #3;
            BCD_in = 4'd15; #3;
    end
endmodule
