`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Team: 3
// Create Date: 05.04.2024 16:15:23
// Experiencie: Guia 1
// Module Name: mux_benchtest
// Description: Testbench para el modulo mux del archivo G1_mux.sv
//


module mux_benchtest();
        logic [3:0] A,B,C,D,out; //Inputs and Outputs
        logic [1:0] sel; //Selector
        
        mux DUT(
        .A (A),
        .B (B),
        .C (C),
        .D (D),
        .sel (sel),
        .out (out)
        );
        
        initial
        begin
        // Declaracion de las entradas
        A = 4'b0011;
        B = 4'b0010;
        C = 4'b0001;
        D = 4'b0000;

        // Declaracion de los selectores

        sel = 2'b00;
        #3
        sel = 2'b01;
        #3
        sel = 2'b10;
        #3
        sel = 2'b11;
        
        end
endmodule
