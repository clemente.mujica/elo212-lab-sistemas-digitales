`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2024 18:56:18
// Design Name: 
// Module Name: sumador_1bit_testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sumador_1bit_testbench();
    logic a, b, cin;
    logic suma, acarreo;
    
    sumador_1bit DUX(
    .a(a),
    .b(b),
    .cin(cin),
    .suma(suma),
    .acarreo(acarreo)
    );
    
    initial
        begin
        a = 1'b0;
        b = 1'b0;
        cin = 1'b0;
        #3
        a = 1'b0;
        b = 1'b0;
        cin = 1'b1;
        #3
        a = 1'b0;
        b = 1'b1;
        cin = 1'b0;
        #3
        a = 1'b0;
        b = 1'b1;
        cin = 1'b1;
        #3
        a = 1'b1;
        b = 1'b0;
        cin = 1'b0;
        #3
        a = 1'b1;
        b = 1'b0;
        cin = 1'b1;
        #3
        a = 1'b1;
        b = 1'b1;
        cin = 1'b0;
        #3
        a = 1'b1;
        b = 1'b1;
        cin = 1'b1;
        end
endmodule
