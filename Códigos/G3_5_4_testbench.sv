`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Team: 3
// Create Date: 
// Experience: Guia 3
// Module Name: Mod7seg_4dis_tb
// Description: Testbench para el m�dulo Mod7seg_4dis



module Mod7seg_4dis_tb;

    // Par�metros de temporizaci�n
    parameter CLOCK_PERIOD = 10; // Periodo del reloj en ns

    // Declaraciones de entradas y salidas del DUT (Device Under Test)
    logic clock, reset;
    logic [6:0] sevenSeg;
    logic [3:0] selectorD;

    // Instanciaci�n del DUT
    Mod7seg_4dis dut (
        .clock(clock),
        .reset(reset),
        .sevenSeg(sevenSeg),
        .SelectorD(selectorD)
    );

    // Generaci�n del reloj
    always #((CLOCK_PERIOD / 2)) clock = ~clock;

    // Inicializaci�n de las se�ales
    initial begin
        clock = 0;
        reset = 1; // Inicia en estado de reset
        #100;     // Espera 100 unidades de tiempo
        reset = 0; // Desactiva el reset

        // Ejemplo de prueba
        // Aqu� puedes cambiar las condiciones de prueba seg�n sea necesario
        // En este ejemplo, cambiamos la entrada cada 200 unidades de tiempo
        repeat (20) begin
            #20; // Espera 20 unidades de tiempo antes de cambiar la entrada
            // Cambiamos la entrada de alguna manera
            // Puedes agregar m�s casos de prueba aqu� seg�n sea necesario

            // Mostrar valores de salida
            $display("bus = %b, D = %b", sevenSeg, selectorD);
        end

        #100; // Espera antes de finalizar la simulaci�n
        $finish; // Finaliza la simulaci�n
    end

endmodule
