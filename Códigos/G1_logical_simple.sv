`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Team: 3 
// Create Date: 05.04.2024 15:07:05
// Experiencie: Guia 1
// Module Name: logical_simple
 
// Tool Versions: 
// Description: 
// Dependencies: 1.0

// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module logical_simple(
    input logic A,B,C,
    output logic X,Y,Z
    );
    assign X = A;
    assign Y = ~A;
    assign Z = B&C;
endmodule
