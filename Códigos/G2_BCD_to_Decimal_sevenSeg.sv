`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiment: Guia 2
// File: BCD_to_sevenSeg.sv
// Teams: 3
// Create Date: 12.04.2024 14:53:53
// Module Name: BCD_to_sevenSeg
// Description: Este modulo convierte un numero BCD a un numero decimal de 0 a 9 dentro de un display de 7 segmentos
// Importante: El display de 7 segmentos esta conectado de la siguiente manera:
//             Catodo comun conectado a Vcc
//             Y anodo va conectado a la salida logica sevenSeg 
//             Los bits de 0 a 6 son a,b,c,d,e,f,g respectivamente
//            BCD_in (BCD = binary coded decimal) es un numero de 4 bits que se desea convertir a 7 segmentos
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BCD_to_sevenSeg(
    input logic [3:0] BCD_in, //BCD = binary coded decimal
    output logic [6:0] sevenSeg
    );
    always_comb begin
        case(BCD_in)           //abcdefg
            4'd0 : sevenSeg = 7'b0000001; // output is abcdefg
            4'd1 : sevenSeg = 7'b1001111;
            4'd2 : sevenSeg = 7'b0010010;
            4'd3 : sevenSeg = 7'b0000110;
            4'd4 : sevenSeg = 7'b1001100;
            4'd5 : sevenSeg = 7'b0100100;
            4'd6 : sevenSeg = 7'b0100000;
            4'd7 : sevenSeg = 7'b0001111;
            4'd8 : sevenSeg = 7'b0000000;
            4'd9 : sevenSeg = 7'b0001100;
            default : sevenSeg = 7'b0110000;
       endcase
    end
endmodule
