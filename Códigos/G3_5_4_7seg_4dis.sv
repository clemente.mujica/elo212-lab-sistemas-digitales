`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experiment: Guia 3
//
// Create Date: 22.04.2024 12:21:44
// Design Name: 
// Module Name: 7seg_4dis

// Description: Es un sistema capaz de mostrar 4 displays de 7 segmentos con multiplexacion temporal y decodificacion binaria.
//              Se utiliza un contador de 16 bits, este mismo esta contenido en un "array" de 4 counters de 4 bits cada uno.
//              Se utiliza un divisor de frecuencia para la multiplexacion de los displays y otro divisor de frecuencia para la actualizacion del contador.
//              Se utiliza un decodificador binario para la seleccion de los displays.
//              Se utiliza un decodificador de binario a decimal para la seleccion de los displays.
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mod7seg_4dis(
    input logic clock, reset, // Señal de reloj y reset
    output logic [6:0] sevenSeg, // Bus de 7 segmentos
    output logic [3:0] SelectorD // Decodificador binario a Hexadecimal
    );
    
    logic clock_outCnt, clock_outSlcnt ;
    logic [3:0] counter1, counter2,counter3,counter4, BDS_in;
    logic [1:0] selector;
    logic reset1, reset2;
    logic [15:0] globalCounter;
    assign globalCounter = {counter1, counter2, counter3, counter4};
    assign reset1 = reset;
    assign reset2 = reset;
    // Archivo de referencia: G3_Divisor_frecuencia.sv
    // Divisor de furecuencia para el contador de 16 bits
        
    Divisor_frecuencia #(100, 25) DIV1( 
        .clk_in(clock),
        .reset(reset),
       .clk_out(clock_outCnt)
    );
    //assign clock_outCnt = clock;
    // Divisor de frecuencia para la seleccion de los displays
    
    /*Divisor_frecuencia #(100, 25) DIV2( 
        .clk_in(clock),
        .reset(reset),
        .clk_out(clock_outSlcnt)
    );
    */
    assign clock_outSlcnt = clock;
    // Archivo de referencia: G2_Counter_Nbits.sv
    // #Parametro 2 bits 

    counter_Nbits #(16) Cnt ( // Contador de 16 bits
        .clk(clock_outCnt), 
        .reset(reset),
        .count(globalCounter)
    );

/* Prueba con numeros definidos
    assign counter1 = 4'b0001;
    assign counter2 = 4'b0010;
    assign counter3 = 4'b0011;
    assign counter4 = 4'b0100;
    */
    
    // Archivo de referencia: G2_Counter_Nbits.sv
    // #Parametro 2 bits 
    
    
    counter_Nbits#(2) Slcnt( // Contador de 2 bits
        .clk(clock_outSlcnt), 
        .reset(reset2),
        .count(selector)
    );
    always_comb // Multiplexacion de los displays
    begin
        case(selector) 
            2'b00 : BDS_in = counter1;
            2'b01 : BDS_in = counter2;
            2'b10 : BDS_in = counter3;
            2'b11: BDS_in = counter4;
            default : BDS_in = 4'b0000;
    endcase;
    end

    // Display de 7 segmentos
    // Archivo de referencia: G3_BCD-7seg16.sv
    BCD_7seg16 Display ( 
        .BCD_in(BDS_in),
        .sevenSeg(sevenSeg)
    );
    
    //Archivo de referencia: G3_Decodificador_Binario.sv
    Decodificador_Binario Eldeco( 
        .a(selector),
        .d(SelectorD)
    );
    
    
    //Un decodificador, pero nose que poner equisde
endmodule
