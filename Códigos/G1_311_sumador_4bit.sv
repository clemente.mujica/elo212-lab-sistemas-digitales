`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2024 18:54:22
// Design Name: 
// Module Name: sumador_4bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sumador_4bit(
    input logic [3:0] a,
    input logic [3:0] b,
    input logic cin,
    output logic [3:0] suma,
    output logic acarreo
    );
    assign {acarreo, suma} = a + b + cin;
endmodule
