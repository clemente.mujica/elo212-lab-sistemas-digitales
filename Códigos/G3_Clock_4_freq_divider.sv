`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience: Guia 3
// Team: 3
// Create Date: 22.04.2024 10:23:32

// Module Name: Clock_freq_divider

// Description: Es un modulo encargado de dividir la frecuencia de la señal de reloj de entrada en 4 señales de reloj de salida con frecuencias de 50Mhz, 30Mhz, 10Mhz y 1Mhz respectivamente
// Caso especial la señal de 30Mhz, ya que no es un divisor exacto de 100Mhz, por lo que se debido a los ciclos de reloj se convierte en un ciclo de 50Mhz

// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Clock_freq_divider(
    input logic clock_100M, reset,
    output logic clock_out_50M, clock_out_30M, clock_out_10M, clock_out_1M
    );
    
    Divisor_frecuencia #(100, 50) D50(
        .clk_in(clock_100M),
        .reset(reset),
        .clk_out(clock_out_50M)
    );
    
    Divisor_frecuencia #(100, 30) D30(
        .clk_in(clock_100M),
        .reset(reset),
        .clk_out(clock_out_30M)
    );
    
    Divisor_frecuencia #(100, 10) D10(
        .clk_in(clock_100M),
        .reset(reset),
        .clk_out(clock_out_10M)
    );
    
    Divisor_frecuenciaN #(100, 1) D1(
        .clk_in(clock_100M),
        .reset(reset),
        .clk_out(clock_out_1M)
    );    
endmodule
