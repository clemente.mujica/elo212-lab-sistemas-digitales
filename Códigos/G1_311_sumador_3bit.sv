`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2024 18:53:08
// Design Name: 
// Module Name: sumador_3bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sumador_3bit(
    input logic [2:0] a,
    input logic [2:0] b,
    input logic cin,
    output logic [2:0] suma,
    output logic acarreo
    );
    assign {acarreo, suma} = a + b + cin;
endmodule
