`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Team: 3
// Create Date: 05.04.2024 16:15:23
// Experiencie: Guia 1 
// Module Name: test_simple
// Description: Testbench para el modulo fib_rec del archivo G2_Fibbionario_Gates.sv
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_simple();

    logic [3:0] BCD_in;
    logic       fib;
    
    fib_rec DUT(
        .BCD_in (BCD_in),
        .fib (fib));
    initial begin
        BCD_in = 4'b0000;
        #3
        BCD_in = 4'b0001;
        #3
        BCD_in = 4'b0010;
        #3
        BCD_in = 4'b0011;
        #3
        BCD_in = 4'b0100;
        #3
        BCD_in = 4'b0101;
        #3
        BCD_in = 4'b0110;
        #3
        BCD_in = 4'b0111;
        #3
        BCD_in = 4'b1000;
        #3
        BCD_in = 4'b1001;
        #3
        BCD_in = 4'b1010;
        #3
        BCD_in = 4'b1011;
        #3
        BCD_in = 4'b1100;
        #3
        BCD_in = 4'b1101;
        #3
        BCD_in = 4'b1110;
        #3
        BCD_in = 4'b1111;
        
     end
endmodule
