`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.04.2024 08:29:39
// Design Name: 
// Module Name: Decodificador_binario_testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Decodificador_binario_testbench();
    logic [1:0] A;
    logic [3:0] D;

    Decodificador_Binario LOL(
        .A(a),
        .D(d)   
    );
    
    initial
    begin
        A = 2'b00;
        #5
        A = 2'b01;
        #5
        A = 2'b10;
        #5
        A = 2'b11;
     end
endmodule
