`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Experience: Guia 2
// Team: 3
// Create Date: 12.04.2024 17:04:14
// Design Name: 
// Module Name: Fibonary_Decimal
// Description:  Este modulo se compone por los modulos counter, BCD_to_sevenSeg y fib_rec,
// los cuales se encargan de contar de 0 a 15 y mostrar el valor en un display de 7 segmentos y 
// de mostrar si el numero es Fibionario o no en un led 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Fibonary_Decimal(
    input logic clock, reset,
    output logic fib, //led 
     on_off, // conexion al catodo comun de el display de 7 segmentos 
    output logic [6:0] bus//seven_Seg a_b_c_d_e_f_g
    
);
    
    assign on_off = 1'b0;
    logic [3:0] count;
    logic [6:0] sevenSeg;
    logic fib;
    
    counter_4bits Count( // Archivo G2_Counter_4bits.sv
        .clk(clock),
        .reset(reset),
        .count(count)
        );
 
    BCD_to_sevenSeg Display( // Archivo G2_BCD_to_Decimal_sevenSeg.sv
        .BCD_in(count),
        .sevenSeg(bus)
    );
    
    fib_rec Fib_Rec( // Archivo G1_fib_rec_Comb.sv
        .BCD_in(count),
        .fib(fib)
    );
        



endmodule
